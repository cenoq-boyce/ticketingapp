import React, { Component } from 'react';
import {
    Switch,
    SliderIOS,
    PickerIOS,
    PickerItemIOS,
    View,
    ScrollView,
    Text,
    Platform,
    StyleSheet,
    WebView,
} from 'react-native';
import ScrollableTabView, {
    DefaultTabBar,
    ScrollableTabBar,
} from 'react-native-scrollable-tab-view';
//var SliderJS = require('react-native-slider')
var styles = require('./styles')
var Button = require('./Button')

//var drawerTypes = ['overlay', 'displace', 'static']

module.exports = React.createClass({
    setParentState(args){
        this.props.setParentState(args)
    },
    render(){
        return (
            <ScrollableTabView
                style={{marginTop: 0, }}
                initialPage={0}
                renderTabBar={() => <DefaultTabBar/>}>

                <WebView tabLabel='Home'
                    source={{uri: 'http://www.cenoq.com'}}
                    style={{marginTop: 20}}/>

                <Text tabLabel='My Tickets'>
                    favorite
                </Text>
            </ScrollableTabView>
        )
    }
})



//// Shadow props are not supported in React-Native Android apps.
//// The below part handles this issue.
//
//// iOS Styles
//var iosStyles = StyleSheet.create({
//    track: {
//        height: 2,
//        borderRadius: 1,
//    },
//    thumb: {
//        width: 30,
//        height: 30,
//        borderRadius: 30 / 2,
//        backgroundColor: 'white',
//        shadowColor: 'black',
//        shadowOffset: {width: 3, height: 5},
//        shadowRadius: 5,
//        shadowOpacity: 0.75,
//    }
//});
//
//var iosMinTrTintColor = '#1073ff';
//var iosMaxTrTintColor = '#b7b7b7';
//var iosThumbTintColor = '#343434';
//
//// Android styles
//var androidStyles = StyleSheet.create({
//    container: {
//        height: 40,
//        justifyContent: 'center',
//    },
//    track: {
//        height: 4,
//        borderRadius: 4 / 2,
//    },
//    thumb: {
//        position: 'absolute',
//        width: 20,
//        height: 20,
//        borderRadius: 20 / 2,
//    },
//    touchArea: {
//        position: 'absolute',
//        backgroundColor: 'transparent',
//        top: 0,
//        left: 0,
//        right: 0,
//        bottom: 0,
//    },
//    debugThumbTouchArea: {
//        position: 'absolute',
//        backgroundColor: 'green',
//        opacity: 0.5,
//    }
//});
//
//var androidMinTrTintColor = '#26A69A';
//var androidMaxTrTintColor = '#d3d3d3';
//var androidThumbTintColor = '#009688';
//
//
//var sliderStyles = (Platform.OS === 'ios') ? iosStyles : androidStyles;
//var minimumTrackTintColor = (Platform.OS === 'ios') ? iosMinTrTintColor : androidMinTrTintColor;
//var maximumTrackTintColor = (Platform.OS === 'ios') ? iosMaxTrTintColor : androidMaxTrTintColor;
//var thumbTintColor = (Platform.OS === 'ios') ? iosThumbTintColor : androidThumbTintColor;